#include "customStrlcpy.h""
size_t strlcpy(char* dst, const char* src, size_t size) {
	--size;
	size_t src_len = strlen(src);
	size_t copy_len = src_len;
	strncpy(dst, src, size);
	dst[size] = '\0';
	return src_len;
}