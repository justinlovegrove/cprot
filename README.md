# c-prototype

A place to prototype some C code.

Currently the only code here is an attempt to implement safe string copies.
Implements a CMake check for strlcpy.
Then defines a custom implementation of strlcpy that I found online.
Uses strlcpy to do copying of char* arrays without users having to specify buffer sizes.
May not be great but gives an initial framework to experiment.