#ifndef STRING_COPIER_H_
#define STRING_COPIER_H_


#define LEN(a) sizeof(a)/sizeof(a[0])

#ifdef CUSTOM_STRLCPY
#include "customStrlcpy.h"
#else
#include <string.h>
#endif

#define customStringCopy(dest,src) strlcpy((dest), (src), LEN(dest))

#endif /*STRING_COPIER_H_*/