#ifndef CUSTOM_STRLCPY_H_
#define CUSTOM_STRLCPY_H_
#include <string.h>
size_t strlcpy(char* dst, const char* src, size_t size);
#endif /*CUSTOM_STRLCPY_H_*/